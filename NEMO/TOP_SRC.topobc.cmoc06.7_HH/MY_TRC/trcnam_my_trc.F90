MODULE trcnam_my_trc
   !!======================================================================
   !!                      ***  MODULE trcnam_my_trc  ***
   !! TOP :   initialisation of some run parameters for LOBSTER bio-model
   !!======================================================================
   !! History :   2.0  !  2007-12  (C. Ethe, G. Madec) Original code
   !!----------------------------------------------------------------------
#if defined key_my_trc
   !!----------------------------------------------------------------------
   !!   'key_my_trc'   :                                       MY_TRC model
   !!----------------------------------------------------------------------
   !! trc_nam_my_trc      : MY_TRC model initialisation
   !!----------------------------------------------------------------------
   USE oce_trc         ! Ocean variables
   USE par_trc         ! TOP parameters
   USE trc             ! TOP variables
!HH0
   USE iom             ! I/O manager
!HH1
   IMPLICIT NONE
   PRIVATE

   PUBLIC   trc_nam_my_trc   ! called by trcnam.F90 module

   !!----------------------------------------------------------------------
   !! NEMO/TOP 3.3 , NEMO Consortium (2010)
   !! $Id: trcnam_my_trc.F90 2528 2010-12-27 17:33:53Z rblod $ 
   !! Software governed by the CeCILL licence (NEMOGCM/NEMO_CeCILL.txt)
   !!----------------------------------------------------------------------

CONTAINS

   SUBROUTINE trc_nam_my_trc
      !!----------------------------------------------------------------------
      !!                     ***  trc_nam_my_trc  ***  
      !!
      !! ** Purpose :   read MY_TRC namelist
      !!
      !!----------------------------------------------------------------------
      !!
      INTEGER :: jl, jn, numnatmyt
      TYPE(DIAG), DIMENSION(jp_my_trc_2d) :: mytdia2d
      TYPE(DIAG), DIMENSION(jp_my_trc_3d) :: mytdia3d
      !!
      NAMELIST/nammytdia/ mytdia3d, mytdia2d     ! additional diagnostics
      !!----------------------------------------------------------------------
      !
      IF(lwp) WRITE(numout,*)
      IF(lwp) WRITE(numout,*) ' trc_nam_my_trc : read MY_TRC namelists'
      IF(lwp) WRITE(numout,*) ' ~~~~~~~~~~~~~~~'
      !
      !                               ! Open the namelist file
      !                               ! ----------------------
      CALL ctl_opn( numnatmyt, 'namelist_my_trc', 'OLD', 'FORMATTED', 'SEQUENTIAL', 1, numout, .FALSE. )
      IF( .NOT.lk_iomput .AND. ln_diatrc ) THEN
         !
         ! Namelist nammytdia
         ! -------------------
         DO jl = 1, jp_my_trc_2d
            WRITE(mytdia2d(jl)%sname,'("2D_",I1)') jl                      ! short name
            WRITE(mytdia2d(jl)%lname,'("2D DIAGNOSTIC NUMBER ",I2)') jl    ! long name
            mytdia2d(jl)%units = ' '                                       ! units
         END DO
         !                                 ! 3D output arrays
         DO jl = 1, jp_my_trc_3d
            WRITE(mytdia3d(jl)%sname,'("3D_",I1)') jl                      ! short name
            WRITE(mytdia3d(jl)%lname,'("3D DIAGNOSTIC NUMBER ",I2)') jl    ! long name
            mytdia3d(jl)%units = ' '                                       ! units
         END DO

         REWIND( numnatmyt )               ! 
         READ  ( numnatmyt, nammytdia )

         DO jl = 1, jp_my_trc_2d
            jn = jp_myt0_2d + jl - 1
            ctrc2d(jn) = mytdia2d(jl)%sname
            ctrc2l(jn) = mytdia2d(jl)%lname
            ctrc2u(jn) = mytdia2d(jl)%units
         END DO

         DO jl = 1, jp_my_trc_3d
            jn = jp_myt0_3d + jl - 1
            ctrc3d(jn) = mytdia3d(jl)%sname
            ctrc3l(jn) = mytdia3d(jl)%lname
            ctrc3u(jn) = mytdia3d(jl)%units
         END DO

         IF(lwp) THEN                   ! control print
            WRITE(numout,*)
            WRITE(numout,*) ' Namelist : natadd'
            DO jl = 1, jp_my_trc_3d
               jn = jp_myt0_3d + jl - 1
               WRITE(numout,*) '  3d diag nb : ', jn, '    short name : ', ctrc3d(jn), &
                 &             '  long name  : ', ctrc3l(jn), '   unit : ', ctrc3u(jn)
            END DO
            WRITE(numout,*) ' '

            DO jl = 1, jp_my_trc_2d
               jn = jp_myt0_2d + jl - 1
               WRITE(numout,*) '  2d diag nb : ', jn, '    short name : ', ctrc2d(jn), &
                 &             '  long name  : ', ctrc2l(jn), '   unit : ', ctrc2u(jn)
            END DO
            WRITE(numout,*) ' '
         ENDIF
         !
      ENDIF
   END SUBROUTINE trc_nam_my_trc
   
#else
   !!----------------------------------------------------------------------
   !!  Dummy module :                                             No MY_TRC
   !!----------------------------------------------------------------------
CONTAINS
   SUBROUTINE trc_nam_my_trc                      ! Empty routine
   END  SUBROUTINE  trc_nam_my_trc
#endif  

   !!======================================================================
END MODULE trcnam_my_trc
