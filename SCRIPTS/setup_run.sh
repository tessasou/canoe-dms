# This script will create copies/links of all necessary files to the EXP## directory of your choice.

############################
# User modification begins:
############################
echo "Enter the cluster name: "
read name_cluster
echo "Enter the CONFIG name: "
read name_config
echo "Enter the EXP number: "
read name_exp
echo "Enter the memo for this run: "
read memo_exp
echo "Enter the spinup year for this run, OR leave it blank if this is an interannual run: "
read spinyear
echo "Enter either NAA1 or NAA6 to indicate the surface ocean layer thickness: "
read naa_n
echo "Enter the frequency of output [e.g. 360 (5-day), 2190 (monthly), 26280 (yearly)]: "
read freout
echo "Enter the total number of CPUs you wish to use for each run: "
read cpus
echo "Enter the year for snow and precip data to be used (e.g. 1985), OR leave it blank if you want to use the daily climatology: "
read snowyear
dir_nemo=$HOME/scratch/canoe-dms
dir_exp=${dir_nemo}/CONFIG/${name_config}/EXP${name_exp}
dir_data=$HOME/data/${name_config}/EXP${name_exp}
# dir_input=$HOME/scratch/input_naa
dir_input=$HOME/scratch/input_eric
dir_xianmin=${dir_input}/xianmin
dir_hhayashi=${dir_input}/hhayashi
dir_tessaou=${dir_input}/tessaou
dir_clark=${dir_input}/clark
dir_ericmort=$HOME/scratch/input_eric
############################
# End of user modification
############################

mkdir -p ${dir_exp}
echo "${dir_exp}: ${memo_exp}" >> ${dir_exp}/../readme.txt

cp -u ${dir_nemo}/SCRIPTS/setup_restart_hh.sh ${dir_exp}
cp -u ${dir_nemo}/CONFIG/cmoc08_naa1/EXP00/namelist* ${dir_exp}
mv ${dir_exp}/namelist_ice_lim2 ${dir_exp}/namelist_ice
ln -s ${dir_nemo}/CONFIG/${name_config}/BLD/bin/nemo.exe ${dir_exp}/opa
# copy the xml stuff (skipped for now).
# cp -u ${dir_nemo}/SCRIPTS/xmlio_server.def ${dir_exp}
# cp -u ${dir_nemo}/SCRIPTS/iodef.xml ${dir_exp}
ln -s ${dir_xianmin}/NAA-I/bathy_level_arc4_masked_jan15.nc ${dir_exp}/bathy_level.nc
ln -s ${dir_xianmin}/NAA-I/bathy_meter_arc4_masked.nc ${dir_exp}/bathy_meter.nc
ln -s ${dir_xianmin}/NAA-I/coordinates_arc4.nc ${dir_exp}/coordinates.nc
#ln -s ${dir_tessaou}/westgrid.cp2WG/forcing/rdic_DTv3vconc.nc ${dir_exp}/river.naa.nc
ln -s ${dir_hhayashi}/naa1/runoff/runoff_bgc_concentration_v3.nc ${dir_exp}/river.naa.nc
ln -s ${dir_xianmin}/NAA-I/core/weight_bilin_core_arc4.nc ${dir_exp}/CORE_weight.nc
ln -s ${dir_xianmin}/NAA-I/weight/*.nc ${dir_exp}
#ln -s ${dir_nemo}/TOOLS/REBUILD/BLD/bin/flio_rbld.exe ${dir_exp}
#ln -s ${dir_nemo}/TOOLS/REBUILD/rebuild ${dir_exp}
#ln -s ${dir_nemo}/TOOLS/UTIL_HPC/srt_data.pisces_NAA ${dir_exp}
#ln -s ${dir_nemo}/TOOLS/UTIL_HPC/clr_link ${dir_exp}
#ln -s ${dir_nemo}/TOOLS/UTIL_HPC/chk_date ${dir_exp}
#ln -s ${dir_hhayashi}/co2_atm/atcco2.txt ${dir_exp}
ln -s ${dir_hhayashi}/co2_atm/atcco2_new.txt ${dir_exp}/atcco2.txt
#ln -s ${dir_hhayashi}/naa1/sss_phc.nc ${dir_exp}
#ln -s ${dir_hhayashi}/piomas/PIOMAS_initialization.nc ${dir_exp}

if [ ${freout} == 2190 ]; then
	freoutsuf=730h
elif [ ${freout} == 360 ]; then
	freoutsuf=5d
elif [ ${freout} == 26280 ]; then
	freoutsuf=1y
fi
vi ${dir_exp}/namelist -c ":%s/\(.*nn_write\s.*=\s\).*\(\s!.*\)/\1${freout}\2/g" -c ":wq"
vi ${dir_exp}/namelist_top -c ":%s/\(.*nn_writetrc\s.*=\s\).*\(\s!.*\)/\1${freout}\2/g" -c ":wq"
vi ${dir_exp}/namelist_top -c ":%s/\(.*nn_writedia\s.*=\s\).*\(\s!.*\)/\1${freout}\2/g" -c ":wq"

if [ "${name_cluster}" == "cedar" ] || [ "${name_cluster}" == "graham" ]; then
	cp -u ${dir_nemo}/SCRIPTS/mpi_job.sh ${dir_exp}
	cp -u ${dir_nemo}/SCRIPTS/sort_output.sh ${dir_exp}
	cp -u ${dir_nemo}/SCRIPTS/interannual_job.sh ${dir_exp}
else
	cp -u ${dir_nemo}/SCRIPTS/qsub_job.pbs ${dir_exp}
	if [ "${name_cluster}" == "nestor" ]; then
		cp ${dir_nemo}/SCRIPTS/qsub_job_nestor.pbs ${dir_exp}/qsub_job.pbs
	fi
	cp -u ${dir_nemo}/SCRIPTS/sort_output.pbs ${dir_exp}
	cp -u ${dir_nemo}/SCRIPTS/q_interannual.sh ${dir_exp}
	vi ${dir_exp}/qsub_job.pbs -c ":%s/\(#PBS -l procs=\).*/\1${cpus}/g" -c ":wq"
	vi ${dir_exp}/qsub_job.pbs -c ":%s/\(cpus=\).*/\1${cpus}/g" -c ":wq"
	vi ${dir_exp}/sort_output.pbs -c ":%s/\(cpus=\).*/\1${cpus}/g" -c ":wq"
	vi ${dir_exp}/sort_output.pbs -c ":%s/\(freoutsuf=\).*/\1${freoutsuf}/g" -c ":wq"
 	if [ "${name_cluster}" == "grex" ]; then #save the output on scratch instead of data.
		vi ${dir_exp}/qsub_job.pbs -c ":%s#\(dir_data=\).*#\1${dir_exp}#g" -c ":wq"
		vi ${dir_exp}/sort_output.pbs -c ":%s#\(dir_data=\).*#\1${dir_exp}#g" -c ":wq"
		vi ${dir_exp}/setup_restart_hh.sh -c ":%s#\(dir_data=\).*#\1${dir_exp}#g" -c ":wq"
	else
		vi ${dir_exp}/qsub_job.pbs -c ":%s#\(dir_data=\).*#\1${dir_data}#g" -c ":wq"
		vi ${dir_exp}/sort_output.pbs -c ":%s#\(dir_data=\).*#\1${dir_data}#g" -c ":wq"
		vi ${dir_exp}/setup_restart_hh.sh -c ":%s#\(dir_data=\).*#\1${dir_data}#g" -c ":wq"
	fi
fi

if [ "${naa_n}" == "NAA6" ]; then
	vi ${dir_exp}/namelist -c ":%s/\(.*rn_hmin\s.*=\s\).*\(\s!.*\)/\1-3.\2/g" -c ":wq" #3 minimum vertical layers
	ln -s ${dir_hhayashi}/naa6/IC/votemper_ORAS4-NAA6_y1969.nc4 ${dir_exp}/data_1m_potential_temperature_nomask.nc
	ln -s ${dir_hhayashi}/naa6/IC/vosaline_ORAS4-NAA6_y1969.nc4 ${dir_exp}/data_1m_salinity_nomask.nc
#ln -s ${dir_xianmin}/NAA-I/Tpot_PHC_arc4.nc ${dir_exp}/data_1m_potential_temperature_nomask.nc
#ln -s ${dir_xianmin}/NAA-I/S_PHC_arc4.nc ${dir_exp}/data_1m_salinity_nomask.nc
	ln -s ${dir_tessaou}/westgrid.cp2WG/forcing/tNAA_GLODAPv2b/3d.clm/alk_GLODAPv2b_shift-NAA_clm1972-2013.msk.ldrown.nc ${dir_exp}/data_Alkalini_nomask.nc
	ln -s ${dir_tessaou}/westgrid.cp2WG/forcing/tNAA_GLODAPv2b/3d.clm/dic_GLODAPv2b_shift-NAA_clm1972-2013.msk.ldrown.nc ${dir_exp}/data_DIC_nomask.nc
	ln -s ${dir_tessaou}/westgrid.cp2WG/forcing/tNAA_GLODAPv2b/3d.clm/no3_GLODAPv2b_shift-NAA_clm1972-2013.msk.ldrown.NONEG.nc ${dir_exp}/data_NO3_nomask.nc
	#ln -s ${dir_hhayashi}/woa13_no3_january_climatology_naa.nc4 ${dir_exp}/data_NO3_nomask.nc
	ln -s ${dir_hhayashi}/naa6/IC/O2_woa13v2-NAA6_.nc4 ${dir_exp}/data_O2_nomask.nc
elif [ "${naa_n}" == "NAA1" ]; then
	 vi ${dir_exp}/namelist -c ":%s/\(.*rn_hmin\s.*=\s\).*\(\s!.*\)/\1-7.\2/g" -c ":wq" #7 min layers
	 ln -s ${dir_hhayashi}/naa1/IC/votemper_ORAS4-NAA1_y1969.nc4 ${dir_exp}/data_1m_potential_temperature_nomask.nc
	 ln -s ${dir_hhayashi}/naa1/IC/vosaline_ORAS4-NAA1_y1969.nc4 ${dir_exp}/data_1m_salinity_nomask.nc
#	 ln -s ${dir_hhayashi}/naa1/IC/votemper_phc_naa6-naa1_.nc4 ${dir_exp}/data_1m_potential_temperature_nomask.nc
#	 ln -s ${dir_hhayashi}/naa1/IC/vosaline_phc_naa6-naa1_.nc4 ${dir_exp}/data_1m_salinity_nomask.nc
	 ln -s ${dir_ericmort}/IC_from_tessa/alk_GLODAPv2b_shift-NAA_naa1_clm1972-2013.fill_em.nc ${dir_exp}/data_Alkalini_nomask.nc
	 ln -s ${dir_ericmort}/IC_from_tessa/dic_GLODAPv2b_shift-NAA_naa1_clm1972-2013.fill_em.nc ${dir_exp}/data_DIC_nomask.nc
	 ln -s ${dir_ericmort}/IC_from_tessa/no3_GLODAPv2b_shift-NAA_naa1_clm1972-2013.fill_em.nc ${dir_exp}/data_NO3_nomask.nc
#	 ln -s ${dir_hhayashi}/naa1/IC/alk_glodap_naa6-naa1_.nc4 ${dir_exp}/data_Alkalini_nomask.nc
#	 ln -s ${dir_hhayashi}/naa1/IC/dic_glodap_naa6-naa1_.nc4 ${dir_exp}/data_DIC_nomask.nc
#	 ln -s ${dir_hhayashi}/naa1/IC/no3_glodap_naa6-naa1_.nc4 ${dir_exp}/data_NO3_nomask.nc
	 ln -s ${dir_hhayashi}/naa1/IC/O2_woa13v2-NAA1_.nc4 ${dir_exp}/data_O2_nomask.nc
fi

#Atmospheric forcing & runoff data & OBC 

abc_core=(PRC Q10 SWDN LWDN T10 SNOW U10 V10)
abc_dfs=(precip q2 radlw radsw snow t2 u10 v10)
len_abc=$(expr ${#abc_core[@]} - 1)

for i in $(seq 1968 2016);
do
 if [[ -z "${spinyear}" ]]; then # Interannual run
  ln -s ${dir_xianmin}/NAArunoff/NAA_runoff_y${i}m00_DaiTrenberth.nc ${dir_exp}/NAA_runoff_y${i}m00_DaiTrenberth.nc
 else # Spinup run
  ln -s ${dir_xianmin}/NAArunoff/NAA_runoff_y${spinyear}m00_DaiTrenberth.nc ${dir_exp}/NAA_runoff_y${i}m00_DaiTrenberth.nc
 fi
 for j in east west;
 do
  # OBC TRC
  if [ "${naa_n}" == "NAA6" ]; then
   ln -s ${dir_hhayashi}/naa6/OBC/obc_${j}_trc_canoe_y0000m00_glodap.NONEG_hh.nc ${dir_exp}/obc_${j}_TRC_y${i}m00.nc
  # OBC TSUV
   for k in TS U V;
   do
    if [[ -z "${spinyear}" ]]; then # Interannual run
	if [ "${i}" == "1968" ]; then
#	ln -s ${dir_xianmin}/NAA-I/OBC/orca_obc_${j}_${k}_y1958m00_arc4.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
		ln -s ${dir_hhayashi}/naa6/OBC/oras4/oras4_obc_${j}_${k}_y1969m00_arc4_naa6.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
	elif [ "${i}" == "2016" ]; then
		ln -s ${dir_hhayashi}/naa6/OBC/oras4/oras4_obc_${j}_${k}_y2015m00_arc4_naa6.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
	else
#	ln -s ${dir_xianmin}/NAA-I/OBC/orca_obc_${j}_${k}_y${i}m00_arc4.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
		ln -s ${dir_hhayashi}/naa6/OBC/oras4/oras4_obc_${j}_${k}_y${i}m00_arc4_naa6.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
	fi
    else # Spinup run
#    ln -s ${dir_xianmin}/NAA-I/OBC/orca_obc_${j}_${k}_y${spinyear}m00_arc4.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
     ln -s ${dir_hhayashi}/naa1/OBC/oras4/oras4_obc_${j}_${k}_y${spinyear}m00_arc4.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
    fi
   done
  elif [ "${naa_n}" == "NAA1" ]; then
#   ln -s ${dir_hhayashi}/naa1/OBC/obc_${j}_trc_canoe_y0000m00_glodap.NONEG_hh_naa1.nc ${dir_exp}/obc_${j}_TRC_y${i}m00.nc
   ln -s ${dir_ericmort}/hhayashi/naa1/OBC/obc_${j}_trc_canoe_y0000m00_glodap.fill_em_naa1.nc ${dir_exp}/obc_${j}_TRC_y${i}m00.nc
   # OBC TSUV
   for k in TS U V;
   do
    if [[ -z "${spinyear}" ]]; then # Interannual run
	if [ "${i}" == "1968" ]; then
#	ln -s ${dir_hhayashi}/naa1/OBC/orca_obc_${j}_${k}_y1958m00_arc4_naa1.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
		ln -s ${dir_hhayashi}/naa1/OBC/oras4/oras4_obc_${j}_${k}_y1969m00_arc4_naa1.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
	elif [ "${i}" == "2016" ]; then
		ln -s ${dir_hhayashi}/naa1/OBC/oras4/oras4_obc_${j}_${k}_y2015m00_arc4_naa1.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
	else
#	ln -s ${dir_hhayashi}/naa1/OBC/orca_obc_${j}_${k}_y${i}m00_arc4_naa1.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
		ln -s ${dir_hhayashi}/naa1/OBC/oras4/oras4_obc_${j}_${k}_y${i}m00_arc4_naa1.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
	fi
    else # Spinup run
#    ln -s ${dir_hhayashi}/naa1/OBC/orca_obc_${j}_${k}_y${spinyear}m00_arc4_naa1.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
     ln -s ${dir_hhayashi}/naa1/OBC/oras4/oras4_obc_${j}_${k}_y${spinyear}m00_arc4_naa1.nc ${dir_exp}/obc_${j}_${k}_y${i}m00.nc
    fi
   done
  fi
 done
 # atmospheric forcing
#if [ "${name_cluster}" == "orcinus" ]; then
# dir_dfs=/global/scratch/steinern
#elif [ "${name_cluster}" == "nestor" ]; then
  dir_dfs=${dir_clark}
#else
# dir_dfs=${dir_hhayashi}
#fi
 for m in $(seq 0 ${len_abc});
 do
  if [[ -z "${spinyear}" ]]; then # Interannual run
   # CORE
#  ln -s ${dir_xianmin}/NAA-I/core/${abc_core[m]}_core_40N_y${i}.nc ${dir_exp}/${abc_core[m]}_core_40N_y${i}.nc
   # DFS
   #snow and precip prior to 1979 are set to a specified year
   if [ "$i" -lt "1979" ]; then
    if [ "${abc_dfs[m]}" == "snow" ] || [ "${abc_dfs[m]}" == "precip" ]; then
     if [[ -z "${snowyear}" ]]; then
      ln -s ${dir_dfs}/DFS/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
     else
      ln -s ${dir_dfs}/DFS/drowned_${abc_dfs[m]}_DFS5.2_y${snowyear}.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
     fi
    else
     ln -s ${dir_dfs}/DFS/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
    fi
   else
     ln -s ${dir_dfs}/DFS/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
   fi
  else # Spinup run
#  ln -s ${dir_xianmin}/NAA-I/core/${abc_core[m]}_core_40N_y${spinyear}.nc ${dir_exp}/${abc_core[m]}_core_40N_y${i}.nc
   ln -s ${dir_dfs}/DFS/drowned_${abc_dfs[m]}_DFS5.2_y${spinyear}.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
  fi
  #replace 1972 and 1976 snow/precip/radsw/radlw data with 1969 data
  #note that for snow/precip, this may be other year specified in snowyear.
  #for radsw/radlw, this overwriting of symbolic links is done to resolve the issue with the data for these years (as shown in Fig.A5 of Hayashida PhD thesis, radsw for 1979/1976 is slightly higher than other years during 1969-1978, although they should all be the same daily climatology (as described in DFS manual).
  if [ "${i}" == "1972" ] || [ "${i}" == "1976" ]; then
   if [ "${abc_dfs[m]}" == "snow" ] || [ "${abc_dfs[m]}" == "precip" ] || [ "${abc_dfs[m]}" == "radsw" ] || [ "${abc_dfs[m]}" == "radlw" ]; then 
    ln -sf ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y1969.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
   fi
  fi
  if [ "${i}" == "1968" ]; then #replace 1957 dataset with 1958 (so that model does not complain to run from year 1958)
   ln -sf ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y1969.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
  elif [ "${i}" == "2016" ]; then #replace 1957 dataset with 1958 (so that model does not complain to run from year 1958)
   ln -sf ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y2015.nc ${dir_exp}/drowned_${abc_dfs[m]}_DFS5.2_y${i}.nc
  fi
 done
done

#the correct runoff data is re-linked in qsub_job.pbs. here the link to 1958 data is made for test running purpose.
ln -sf NAA_runoff_y1969m00_DaiTrenberth.nc ${dir_exp}/runoff_1m_nomask.nc

cd ${dir_exp}
